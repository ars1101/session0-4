import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:pinput/pinput.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';

class otp extends StatefulWidget {
  const otp({super.key});

  @override
  State<otp> createState() => _otpState();
}

User? user;

class _otpState extends State<otp> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController email = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if (
    (email.text.length == 6)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'OTP Verification',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Enter the 6 digit numbers sent to your email',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 52,
                ),

                SizedBox(
                    height: 32,
                    width: MediaQuery.of(context).size.width - 48,
                    child: Pinput(defaultPinTheme: PinTheme(width: 32, height: 32, decoration: BoxDecoration(borderRadius: BorderRadius.zero),

                    ),
                    separatorBuilder: (context) => SizedBox(width: 30,),
                      submittedPinTheme: PinTheme(width: 32, height: 32, decoration: BoxDecoration(borderRadius: BorderRadius.zero,),

                      ), controller: email,
                    )),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 56,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (isValid() == true) ? null : () async {try{
                      sendOtp(email.text);
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => otp()));
                    } on AuthException catch(e){
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
                    }
                    },
                    child: Text('Set New Password'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Remember password? Back to',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => SignIn()));
                        },
                        child: Text(
                          'Sign in',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color(0xFF0560FA)),
                        ),
                      )
                    ],
                  ),

                ),
                SizedBox(height: 28,)
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}
