import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/main.dart';
import 'package:session0_4/onboarding/presentation/widgets/customcard.dart';
import 'package:session0_4/onboarding/presentation/widgets/textfield.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';
import 'order.dart';

class Order extends StatefulWidget {
  const Order({super.key});

  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {
  bool obs = true;
  bool check = false;
  double balance = 0;
  String name = '';
  bool value = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      getUser().then((value) => {
        setState(() {
          balance = value["balance"];
          name = value["fullname"].toString();
          print(value);
        })
      });
    });
  }

  int ind = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: Column(children: [
                SizedBox(
                  height: 69,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Image.asset('assets/arrow_2.png')),
                    SizedBox(
                      width: 60,
                    ),
                    Text(
                      'Send Package',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Color(0xFFA7A7A7),
                )
              ])),
          SliverList.builder(itemCount: itemcount,
              itemBuilder: (_, index){
                return Row(
                  children: [
                    SizedBox(
                      width: 24,
                    ),
                    Column(
                      children: [
                        Text(dest[ind],
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Color(0xFFA7A7A7))),
                        Text(pnes[ind],
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Color(0xFFA7A7A7))),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                  ],
                );
    }

          )],
      ),
    );
  }
}
