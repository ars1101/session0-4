// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session0_4/core/myapp.dart';

import 'package:session0_4/main.dart';
import 'package:session0_4/onboarding/data/repository/origindata.dart';
import 'package:session0_4/onboarding/data/repository/queue.dart';
import 'package:session0_4/onboarding/presentation/pages/holder.dart';
import 'package:session0_4/onboarding/presentation/pages/onboarding.dart';

void main() {
  Queue queue = Queue();
 for(var i in originData){
   queue.push(i);
 }




  testWidgets(
      '−	Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).', (
      WidgetTester tester) async {
    queue.resetQueue;
    for (Map<String, String> value in originData) {
      expect(value, queue.next());
    }
  });

  testWidgets('−	Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', (WidgetTester tester) async {
    queue.resetQueue();
    var originLength = queue.length();
    queue.next();
    expect(queue.length(), originLength-1);

  });

  testWidgets('−	В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.', (WidgetTester tester) async {
    queue.resetQueue;
    final pageView = find.byType(PageView);

    await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
      home: OnBoarding(),
    )));

    var elem = queue.next();
    await tester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);

    final titleText = find.text(elem["title"]!);
    await tester.dragUntilVisible(titleText, pageView, const Offset(-250, 0));

    elem = queue.next();
    await tester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);

  });

  testWidgets('−	Случай, когда очередь пустая, надпись на кнопке должна измениться на "Sing Up".', (WidgetTester tester) async {
    queue.resetQueue;
    final pageView = find.byType(PageView);

    await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
      home: OnBoarding(),
    )));

   while(queue.length() > 1){
     var elem = queue.next();
     if(queue.length() == 0){
       await tester.pumpAndSettle();
     expect(await find.widgetWithText(FilledButton, "Sign up"), findsOneWidget);}
   }

  });

  testWidgets('−	Если очередь пустая и пользователь нажал на кнопку “Sing in”, происходит открытие пустого экрана «Holder» приложения. Если очередь не пустая – переход отсутствует.', (WidgetTester tester) async {
    queue.resetQueue;

    while(queue.length() > 1){
      var elem = queue.next();
      if(queue.length() == 0) {
        await tester.pumpAndSettle();
        await tester.tap(find.widgetWithText(FilledButton, "Sign Up"));
        expect(find.byElementType(Holder), findsOneWidget);
      }
  }});
}
