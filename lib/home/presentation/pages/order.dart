import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/main.dart';
import 'package:session0_4/onboarding/presentation/widgets/customcard.dart';
import 'package:session0_4/onboarding/presentation/widgets/textfield.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';

class Order extends StatefulWidget {
  const Order({super.key});

  @override
  State<Order> createState() => _OrderState();
}
TextEditingController ora = TextEditingController();
TextEditingController ors = TextEditingController();
TextEditingController orp = TextEditingController();
TextEditingController oro = TextEditingController();
TextEditingController pi = TextEditingController();
TextEditingController wei = TextEditingController();
TextEditingController wor = TextEditingController();
String code = '';
User? user;
int itemcount = 1;
List<String> dest = [];
List<String> pnes = [];

class _OrderState extends State<Order> {
  bool obs = true;
  bool check = false;
  double balance = 0;
  String name = '';
  bool value = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      getUser().then((value) => {
            setState(() {
              balance = value["balance"];
              name = value["fullname"].toString();
              print(value);
            })
          });
    });
  }

  int ind = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                SizedBox(
                  height: 69,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Profile",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Color(0xFFA7A7A7),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: Row(
              children: [
                SizedBox(
                  width: 24,
                ),
                Column( crossAxisAlignment: CrossAxisAlignment.start,
                  children: [SizedBox(height: 43,),
                    Row(
                      children: [
                        Image.asset('assets/origin.png'),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          'Origin Details',
                          style: TextStyle(
                              color: Color(0xFF3A3A3A),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.start,
                    ),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Address', controller: ora),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'State,Country', controller: ors),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Phone number', controller: orp),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Others', controller: oro),),
                    SizedBox(height: 39,),


                  ],
                ),
                SizedBox(
                  width: 24,
                )
              ],
            ),
          ),
          SliverList.builder(itemBuilder: (_,index){
            dest.add('');
            pnes.add('');
            TextEditingController adr = TextEditingController();
            TextEditingController phn = TextEditingController();
            TextEditingController state = TextEditingController();
            TextEditingController oth = TextEditingController();
            return Row(
              children: [SizedBox(width: 24,),
                Column(crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset('assets/dest.png'),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          'Destination Details',
                          style: TextStyle(
                              color: Color(0xFF3A3A3A),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.start,
                    ),
                    SizedBox(height: 4,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Address', controller: adr, onChanged: (tx) {
                      dest[index] = (index + 1).toString() + '. ' + adr.text + state.text;
                    },),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'State,Country', controller: state, onChanged: (tx) {
                dest[index] = (index + 1).toString() + '. ' + adr.text + state.text;
                }),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Phone number', controller: phn,onChanged: (tx) {
                      pnes[index] = phn.text;
                    }),),
                    SizedBox(height: 5,),
                    SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Others', controller: oth,onChanged: (tx) {
                      dest[index] = (index + 1).toString() + '. ' + adr.text + state.text;
                    }),),
                    SizedBox(height: 39,),

                  ],
                ),SizedBox(width: 24,)
              ],
            );
          }, itemCount: itemcount,),
          SliverToBoxAdapter(child: Row(mainAxisAlignment: MainAxisAlignment.start,children: [SizedBox(width: 24,),
          Column(children: [

            Row(
              children: [
                SizedBox(
                  width: 24,
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        itemcount = itemcount + 1;
                      });
                    },
                    icon: Image.asset('assets/add.png')),
                SizedBox(
                  width: 6,
                ),
                Text(
                  'Add Destination',
                  style: Theme.of(context).textTheme.titleSmall,
                )
              ],
            ),
            Text(
            'Origin Details',
            style: TextStyle(
                color: Color(0xFF3A3A3A),
                fontSize: 14,
                fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 8,),
            SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'package items', controller: pi,), ),
            SizedBox(height: 8,),
            SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Weight of item(kg)', controller: wei,), ),
            SizedBox(height: 8,),
            SizedBox(width: MediaQuery.of(context).size.width-48, height: 32, child: OrderTextField(hint: 'Worth of Items', controller: wor,), ),
            SizedBox(height: 39,)

          ], crossAxisAlignment: CrossAxisAlignment.start,),
          SizedBox(width: 24,)

          ],),),
          SliverToBoxAdapter(
            child: Row(
              children: [
                SizedBox(
                  height: 75,
                  width: 169,
                  child: FilledButton(
                      onPressed: () async {

                        code = await sendOrder(user!.id!, ora.text, ors.text, orp.text, oro.text, pi.text, wei.text, wor.text, itemcount);

                      },
                      child: Column(
                        children: [
                          Image.asset('assets/clock.png'),
                          SizedBox(
                            height: 10,
                          ),
                          Text('Instant delivery', style: TextStyle(fontSize: 14),)
                        ],
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                      )),
                ),
                SizedBox(
                  width: 24,
                ),
                SizedBox(
                    height: 75,
                    width: 169,
                    child: FilledButton(
                      onPressed: () {},
                      child: Column(
                        children: [
                          Image.asset('assets/calen.png'),
                          SizedBox(
                            height: 10,
                          ),
                          Text('Scheduled delivery', style: TextStyle(fontSize: 14),)
                        ],
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                      style: FilledButton.styleFrom(
                          elevation: 5,
                          backgroundColor: Colors.white,
                          foregroundColor: Color(0xFFA7A7A7)),
                    )),

              ],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          )
        ],
      ),
    );
  }
}
