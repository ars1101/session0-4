
import 'package:session0_4/onboarding/data/repository/origindata.dart';

class Queue{
  final List<Map<String,String>> _data = [];

  void push(Map<String, String> elem){
  var count = _data.length;

  _data.insert(count, elem);
  }

  Map<String, String> next(){
    var elem = _data[0];
    for (var i in _data.reversed){
      elem = i;
    }
    _data.remove(elem);
    return elem;

  }

  bool isEmpty(){
    var count = _data.length;

    return count == 0;

    return false;
  }

  int length(){
    return _data.length;
  }

  void resetQueue(){
    _data.clear();
      _data.addAll(originData);


}}