import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/home/presentation/pages/order.dart';
import 'package:session0_4/home/presentation/pages/profile.dart';
import 'package:session0_4/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

User? user;

class _HomeState extends State<Home> {
  @override
  void initState() {}

int ind = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(currentIndex: ind,
          items: [BottomNavigationBarItem(
            icon: Image.asset(
                (ind == 0) ? "assets/house-2-1.png" : "assets/house-2.png"),
            label: "Home",
          ),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 1)?'assets/wallet-3.png': 'assets/wallet-3.png' ), label: "Wallet"),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 2)
                    ? "assets/smart-car-1.png"
                    : "assets/smart-car.png"),
                label: "Tracking"),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 3)
                    ? "assets/profile-circle-1.png"
                    : "assets/profile-circle.png"),
                label: "profile")],
          selectedItemColor: (Color(0xFF0560FA)),
          unselectedItemColor: Color(0xFFA7A7A7),
          unselectedLabelStyle: TextStyle(color: Color(0xFFA7A7A7)),
          selectedLabelStyle: TextStyle(color: Color(0xFF0560FA)),
          showUnselectedLabels: true,
          showSelectedLabels: true,
          onTap: (inda) {
            setState(() {
              ind = inda;
            });
          }),
      body: [Center(child: FilledButton(child: Text('To Order'), onPressed: (){Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => Order()));},),), SizedBox(), SizedBox(), Profile()][ind],
    );
  }
}
