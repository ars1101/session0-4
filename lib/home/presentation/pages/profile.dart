import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/main.dart';
import 'package:session0_4/onboarding/presentation/widgets/customcard.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

User? user;

class _ProfileState extends State<Profile> {
  bool obs = true;
  bool check = false;
  double balance = 0;
  String name = '';
  bool value = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      getUser().then((value) => {
            setState(() {
              balance = value["balance"];
              name = value["fullname"].toString();
              print(value);
            })
          });
    });
  }

  int ind = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(children: [
                SizedBox(
          height: 69,
                ),
                Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Profile",
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
                ),
                SizedBox(
          height: 15,
                ),
                Container(
          width: MediaQuery.of(context).size.width,
          height: 1,
          color: Color(0xFFA7A7A7),
                ),
                SizedBox(
          height: 27,
                ),
                Row(
          children: [
            SizedBox(
              width: 20,
            ),
            SizedBox(
              height: 60,
              width: 60,
              child: Image.asset(
                "assets/ken.png",
                scale: 0.4,
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hello " + name.toString(),
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                ),
                Row(
                  children: [
                    Text(
                      'Current balance: ',
                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                    ),
                    Text(
                      'N' + balance.toString(),
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF0560FA)),
                    ),
                    SizedBox(
                      width: 111,
                    ),
                  ],
                ),
                SizedBox(
                  height: 19,
                ),
              ],
            ),
          ],
                ),
                SizedBox(
          height: 19,
                ),
                Row(
          children: [
            SizedBox(
              width: 24,
            ),
            Text(
              "Enable Dark Theme",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            Spacer(),
            Switch(
                value: value,
                onChanged: (val) {
                  setState(() {
                    value = val;
                  });
                }),
            SizedBox(
              width: 27,
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
                ),
                SizedBox(
          height: 19,
                ),
                SizedBox(
            height: 62,
            width: MediaQuery.of(context).size.width - 48,
            child: CustomCard(
                image: 'assets/prof.png',
                hint: 'Name, phone no, address, email ...',
                label: 'Edit profile')),
                SizedBox(
          height: 12,
                ),
                SizedBox(
            height: 62,
            width: MediaQuery.of(context).size.width - 48,
            child: CustomCard(
              image: 'assets/stat.png',
              label: 'Statements & Reports',
              hint: 'Download transaction details, orders, deliveries',
            )),
                SizedBox(
          height: 12,
                ),SizedBox(
                height: 62,
                width: MediaQuery.of(context).size.width - 48,
                child: CustomCard(
                  image: 'assets/not.png',
                  label: 'Notification Settings',
                  hint: 'mute, unmute, set location & tracking setting',
                )),
            SizedBox(
              height: 12,
            ),
            SizedBox(
                height: 62,
                width: MediaQuery.of(context).size.width - 48,
                child: CustomCard(
                  image: 'assets/not.png',
                  label: 'Card & Bank account settings',
                  hint: 'change cards, delete card details',
                )),
            SizedBox(
              height: 12,
            ),
            SizedBox(
                height: 62,
                width: MediaQuery.of(context).size.width - 48,
                child: CustomCard(
                  image: 'assets/ref.png',
                  label: 'Refferals',
                  hint: 'check no of friends and earn',
                )),
            SizedBox(
              height: 12,
            ),
            SizedBox(
                height: 62,
                width: MediaQuery.of(context).size.width - 48,
                child: CustomCard(
                  image: 'assets/map.png',
                  label: 'About Us',
                  hint: 'know more about us, terms and conditions',
                )),
            SizedBox(
              height: 12,
            ),
            GestureDetector( onTap: (){supabase.auth.signOut();
            },
              child: SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/logout.png',
                    label: 'Log Out',
                    hint: '',
                  )),
            ),
            SizedBox(
              height: 12,
            ),
              ]),
        ));
  }
}
