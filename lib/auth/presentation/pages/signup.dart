import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session0_4/auth/presentation/pages/signin.dart';
import 'package:session0_4/auth/presentation/widgets/textfield.dart';
import 'package:session0_4/core/supawidgets.dart';
import 'package:session0_4/home/presentation/pages/home.dart';
import 'package:session0_4/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:email_validator/email_validator.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

User? user;
User? usera;

class _SignUpState extends State<SignUp> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController passc = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if ((name.text.length > 0) &
        (phone.text.length > 0) &
        (email.text.length > 0) &
        (pass.text.length >= 6) &
    check &
        (pass.text == passc.text)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'Create an account',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Complete the sign up process to get started',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 33,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChange: (te){setState(() {});},
                        label: 'Full Name',
                        hint: 'Ivanov Ivan',
                        isObscure: false,
                        controller: name)),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 72,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomTextField(
                      label: 'Phone Number',
                      hint: '+7(999)999-99-99',
                      isObscure: false,
                      formatter: MaskTextInputFormatter(
                          mask: '+#(###)###-##-##',
                          filter: {'#': RegExp(r'[0-9]')}),
                      controller: phone),
                ),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChange: (te){setState(() {});},
                        label: 'Email Address',
                        hint: '***********@mail.com',
                        isObscure: false,
                        controller: email)),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChange: (te){setState(() {});},
                        label: 'Password',
                        hint: '**********',
                        isObscure: obs,
                        controller: pass,
                        onTapSuffix: () {
                          setState(() {
                            if (obs == true) {
                              obs = false;
                            } else {
                              obs = true;
                            }
                          });
                        })),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField( onChange: (te){setState(() {});},
                      label: 'Confirm Password',
                      hint: '**********',
                      isObscure: obs,
                      controller: passc,
                      onTapSuffix: () {
                        setState(() {
                          if (obs == true) {
                            obs = false;
                          } else {
                            obs = true;
                          }
                        });
                      },
                    )),
                SizedBox(
                  height: 37,
                ),
                Row(
                  children: [
                    SizedBox(
                      height: 14,
                      width: 14,
                      child: Checkbox(
                          value: check,
                          onChanged: (val) {
                            setState(() {
                              check = val!;
                            });
                          }),
                    ),
                    SizedBox(
                      width: 11,
                    ),
                    GestureDetector(
                      onTap: () async {
                        await launchUrl(pdf);
                      },
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: 'By ticking this box, you agree to our ',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 167, 167, 167),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500),
                              children: [
                                TextSpan(
                                    text:
                                        "Terms and\nconditions and private policy",
                                    style: TextStyle(
                                        color: Color(0xFFEBBC2E),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500)),
                              ])),
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.start,
                ),
                SizedBox(
                  height: 64,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (!isValid()) ? null : () async {try{
                      var res = await regIn(name.text, pass.text, email.text, phone.text);

                      if(res != null){user = res!;
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => Home()));
                        }
                    } on AuthException catch(e){
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message)));
                    }
                    },
                    child: Text('Sign Up'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => SignIn()));
                        },
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color(0xFF0560FA)),
                        ),
                      )
                    ],
                  ),

                ),
                SizedBox(height: 18,),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'or sign in using',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),],
                  ),


                ), SizedBox(height: 8,),
                SizedBox(width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/google.png'),],
                  ),


                ),
                SizedBox(height: 28,)
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}
