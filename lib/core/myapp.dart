import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:session0_4/core/themes.dart';
import 'package:session0_4/onboarding/presentation/pages/onboarding.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: Themed,
      home: OnBoarding(),
    );
  }
}