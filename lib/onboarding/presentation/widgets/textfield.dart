import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class OrderTextField extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final Function? onChanged;

  OrderTextField({
    required this.hint,
    required this.controller,
    this.onChanged

  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container( decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Color(
            0x25000000), offset: Offset(0,2), blurRadius: 5)]),
          height: 32,
          width: MediaQuery.of(context).size.width - 48,
          child: TextField( onChanged: (onChanged != null)? (x){
            onChanged!(x);
          }: null,
            controller: controller,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: ' ' + hint,
                hintStyle: Theme.of(context).textTheme.titleSmall
            ),
          ),
        ));
  }
}